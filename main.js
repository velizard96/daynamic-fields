$(document).ready(function () {

  let dynamicFieldsContent = {
    "invoice": [
      {
        "name": "price",
        "type": "number",
        "label": "Price"
      },
      {
        "name": "company_name",
        "type": "text",
        "label": "Company name"
      },
      {
        "name": "vat_percent",
        "type": "number",
        "label": "Vat percent"
      }
    ],
    "message": [
      {
        "name": "for",
        "type": "text",
        "label": "For"
      },
      {
        "name": "message",
        "type": "textarea",
        "label": "Message"
      }
    ],
    "application": [
      {
        "name": "city",
        "type": "text",
        "label": "City"
      },
      {
        "name": "street",
        "type": "text",
        "label": "Street"
      },
      {
        "name": "building",
        "type": "text",
        "label": "Building"
      }
    ]
  }

  let field = $('#type').val()
  let fields = dynamicFieldsContent[field]
  for (let field of fields) {
    createField(field)
  }

  $('#type').on("change", function () {
    $('#generated-fields').empty()
    
    let field = $(this).val()
    let fields = (dynamicFieldsContent[field])

    for (let field of fields) {
      createField(field)
    }
  })

  function createField(field) {

    let formGroup = $('<div>')
    formGroup.addClass('form-groups')

    let newField = $('<input>')

    let newLabel = $('<label>')
    newLabel.text(field.label)

    newField.addClass('form-inputs')
    newField.attr('name', field.name)

    if (field.type === 'textarea') {
      newField = $('<textarea>')
      newField.addClass('form-inputs')
    } else {
      newField.attr('type', field.type)
    }

    formGroup.append(newLabel)
    formGroup.append(newField)

    $('#generated-fields').append(formGroup)

  }

})
